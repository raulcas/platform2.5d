﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Player : MonoBehaviour
{
    private Vector2 axis;
    private Rigidbody rb;
 
    private int jumping;
    private float time;
    [SerializeField] int maxJumps;
    [SerializeField] float tiempoEntreSalto;
 
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        jumping = 0;
        time = 0f;
    }
   
    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        time += Time.deltaTime;
        //Movimiento Horizontal
        rb.velocity = new Vector3(axis.x*6 , rb.velocity.y, 0);
 
        //Movimiento Vertical
        if (jumping<maxJumps && axis.y > 0.2f && time> tiempoEntreSalto) {
            time = 0f;
            jumping++;
            rb.AddForce(Vector3.up * 6, ForceMode.Impulse);
        }
    }
 
    public void SetAxis(Vector2 input){
        axis = input;
    }
 
 
    private void OnCollisionEnter(Collision collision)
    {
        jumping = 0;
    }
 
}